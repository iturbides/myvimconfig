Hi, this is my Vim configuration file
<img src="vim.png">

Vim is my default text editor for web development and writing.  That is why my .vimrc is adapted for both.

Here are some points:

1. Numbered lines only appear if the document extension corresponds to a language (html, css, javascript, liquid, etc.).
2. Liquid is treatated like Html
3. In MD and TXT extensions there are no numbered lines, and use wrap soft
4. The coc plugin is active for html, css, javascript, etc.

Some Commands
I have configured the following shortcuts:

1. F2 to switch between buffers (if there are changes save them before changing).
2. F6 to close the current buffer
3. F3 enables and highlighter in search
4. Ctrl-n enable disable lines numbered
5. Ctrl-t run :Explore


