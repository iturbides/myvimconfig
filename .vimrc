
syntax on
set number
set belloff=all
set signcolumn=no
set hlsearch!
set nobackup
set nowritebackup
set showmatch
set clipboard=unnamed
set spelllang=es
set encoding=UTF-8
set linebreak
set mouse=a
set laststatus=1
set foldcolumn=1
set t_Co=256
:set omnifunc=javascriptcomplete#CompleteJS
let g:user_emmet_leader_key=','
let g:netrw_list_hide = '^\..*'
let g:netrw_keepdir = 0
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_flow = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='cobalt2'
let g:airline_powerline_fonts = 1
let g:closetag_filenames = '*.html,*.xhtml,*.js'
let g:coc_global_extensions = [
  \ 'coc-html',
  \ 'coc-css',
  \ 'coc-json',
  \ ]
call plug#begin()
Plug 'sheerun/vim-polyglot'
Plug 'ternjs/tern_for_vim', { 'do' : 'npm install' }
Plug 'pangloss/vim-javascript'
Plug 'tpope/vim-fugitive'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ap/vim-css-color'
Plug 'reedes/vim-pencil'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'alvan/vim-closetag'
Plug 'valloric/matchtagalways'
Plug 'ryanoasis/vim-devicons'
Plug 'tpope/vim-liquid'
Plug 'mattn/emmet-vim'
call plug#end()

nnoremap <C-t> :Explore<CR>
nnoremap <C-n> :set number!<CR>
nnoremap <F6> :bw<CR>
nnoremap <F3> :set hlsearch!<CR>
if &t_Co > 2
         set background=dark
         highlight FoldColumn cterm=NONE ctermbg=0 ctermfg=0
         highlight Conceal cterm=NONE ctermbg=NONE ctermfg=0
 endif
nnoremap  <silent>   <F2>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-F2>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>


augroup pencil
  autocmd!
  autocmd FileType markdown,md call pencil#init({'wrap': 'soft'})
  autocmd FileType text         call pencil#init({'wrap': 'soft'})
augroup end

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType php,xml,json,html,css,js,liquid,py setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

autocmd FileType md,markdown,txt set nonumber
" autocmd FileType php,xml,json,html,css,js,scss,xml,liquid set number

" Formato para html del Emmet
let g:user_emmet_settings = {
\  'variables': {'lang': 'es'},
\  'html': {
\    'default_attributes': {
\      'option': {'value': v:null},
\      'textarea': {'id': v:null, 'name': v:null, 'cols': 10, 'rows': 10},
\    },
\    'snippets': {
\      'html:5': "<!DOCTYPE html>\n"
\              ."<html lang=\"${lang}\">\n"
\              ."<head>\n"
\              ."\t<meta charset=\"${charset}\">\n"
\              ."\t<title></title>\n"
\              ."\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
\		."\t<meta name=\"description\" content=\"\">\n"
\ 		."\t<meta name=\"keywords\" content=\"\">\n"
\		."\t<meta rel=\"icon\" href=\"\">\n"
\              ."</head>\n"
\              ."<body>\n\t${child}|\n</body>\n"
\              ."</html>",
\    },
\  },
\}
filetype plugin on
set omnifunc=syntaxcomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
